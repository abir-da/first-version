import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import { Storage  } from '@google-cloud/storage'; 
import Jimp from 'jimp'
import admin  from 'firebase-admin';
const app = express();
dotenv.config();
app.use(express.json());
app.use(cors());
const storage = new Storage({
  
    keyFilename: "./version-0-4a2ad-firebase-adminsdk-h6q32-7a9beddf63.json",
 });

let bucketName = "version-0-4a2ad.appspot.com"
let file_name="about.png"
let filename = `images/${file_name}`

const downloadFile = () => {
    let destFilename = `./${filename}`;
    const options = {
      // The path to which the file should be downloaded, e.g. "./file.txt"
      destination: destFilename,
    };

    // Downloads the file
    storage.bucket(bucketName).file(filename).download(options);

    console.log(
      `gs://${bucketName}/${filename} downloaded to ${destFilename}.`
    );
    console.log(`${filename} uploaded to ${bucketName}.`);
    
  } 

// Connexion à la base données
/*mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log("Connexion à la base de données réussie");
    }).catch(err => {
        console.log('Impossible de se connecter à la base de données', err);
        process.exit();
    });

app.use('/api/images', ImageRouter);*/


const flip = async()=> { 
   const image = await Jimp.read (`./images/${file_name}`)
   image.flip(true, false, function(err){
      if (err) throw err;
   })
   .write(`./images-flip/${file_name}`)
}

downloadFile()
flip()

const uploadFile = () => {
  admin.initializeApp({
    serviceAccountId: '109243248117797505938@version-0-4a2ad.iam.gserviceaccount.com',
  });
 storage.bucket(bucketName).upload(`./images-flip/${file_name}`, {
    
      public: true,
      destination: `images-flip/${file_name}`,
   
      metadata: {
          cacheControl: 'public, max-age=31536000',
      },
});
console.log(`${filename} uploaded to ${bucketName}.`);
}
uploadFile()
app.listen(process.env.PORT, () => {
    console.log(`Server is listening on port ${process.env.PORT}`);

  });


