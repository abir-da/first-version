//import mongoose from 'mongoose';
import Image from '../model/image.js';
export const getImages = async (req, res) => {
    try {
        const cat = await Image.find();
        res.status(200).json(cat);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}
export const createImage = async (req, res) => {
    const { nomimage, url } = req.body;
    const newImage = new Image({ nomimage: nomimage, url: url })
    try {
        await newImage.save();
        res.status(201).json(newImage);
    } catch (error) {
        res.status(409).json({ message: error.message });
    }
}
