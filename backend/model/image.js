import mongoose from "mongoose"
var imageSchema = mongoose.Schema({
    nomimage: String,
    url: String,
});
const Image = mongoose.model('Image', imageSchema);
export default Image