import React,{useState} from "react";
import { storage } from "./firebase";
//import axios from "axios";
const ReactFirebaseUploadFile = () => {
    const [image,setImage] = useState(null);
    //const [nomimage,setnomimage] = useState();
    const [url,seturl] = useState("");
    const [progress, setProgress] = useState(0);
    /*const objetimage = {
        nomimage: nomimage,
        url :url
        };*/
    const handleChange = e =>{
        if(e.target.files[0]){
            setImage(e.target.files[0]);
            //setnomimage(e.target.files[0].name);
            //seturl("images/"+e.target.files[0].name);
        }
    }
    const handleUpload=()=>{
        const uploadTask = storage.ref(`images/${image.name}`).put(image);
        uploadTask.on(
            "state_changed",
            snapshot => {
                const prog = Math.round((snapshot.bytesTransferred / snapshot.totalBytes)*100);
                setProgress(prog);
            },
            error => {
                console.log(error);
            },
            () => {
                storage
                    .ref("images")
                    .child(image.name)
                    .getDownloadURL()
                    .then(url => {
                        seturl(url);
                    })
            }
        )
       /* try {
            axios.post(process.env.REACT_APP_API_URL+"/images",objetimage);
            
        } catch (error) {
            console.log(error);
            
        }*/
        
    };

    console.log("image : ", image);
  return (
    <div>
        Insert an image SVP
        <br />
        <br />
        <input type="file" onChange={handleChange} />
        <button onClick={handleUpload}>Upload</button> 
        <br />
        <hr />
        <h3>Uploded {progress} %</h3>
        <hr/>
        
        
    </div>
  );
}

export default ReactFirebaseUploadFile;
