import "./App.css";
import ReactFirebaseUploadFile from "./ReactFirebaseUploadFile";


function App() {
  return (
    <div className="App">
      <ReactFirebaseUploadFile/>
    </div>
  );
}

export default App;
